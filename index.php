<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="dist/bootstrap.css">
    <link rel="stylesheet" href="dist/font-crimson.css">
    <link rel="stylesheet" href="dist/index_style.css">
    <link rel="stylesheet" href="src/css/all.css">
    <style>
        .navbar-fixed-top.scrolled {
            background-color: #000 !important;
            transition: background-color 200ms linear;
        }

        .navbar-fixed-top.scrolled .nav-link {
            color:#555;
        }
    </style>
</head>
<body>
    <?php 
        $data = array(
            [
                'title' => 'Coffee',
                'bg_url' =>  'Gallery-1.jpg'
            ],
            [
                'title' => 'Breakfast',
                'bg_url' =>  'Gallery-3.jpg'
            ],
            [
                'title' => 'Sandwich',
                'bg_url' =>  'Gallery-4.jpg'
            ],
            [
                'title' => 'Juice',
                'bg_url' =>  'Gallery-2.jpg'
            ]
        );
    ?>
    <div class="row" style="background-image: url(src/images/Header-Image.jpg); background-repeat: no-repeat; height: 960px; width: 1360px; background-size: contain; margin-bottom: 100px;">
        <div class="row" style="position: fixed;">
            <div class="col-lg-12">
                <nav class="navbar navbar-expand-lg navbar-light navbar-fixed-top" style="width: 1348px;">
                    <div class="container-fluid">
                        <img src="src/images/logo.svg" alt="" srcset="" style="margin-left: 80px; width: 80px; height: 80px;">
                        <div class="d-flex">
                            <a class="navbar-brand" href="#" style="font-size: inherit; margin-right: 75px; color: white;">ABOUT</a>
                            <a class="navbar-brand" href="#" style="font-size: inherit; margin-right: 75px; color: white;">MENU</a>
                            <a class="navbar-brand" href="#" style="font-size: inherit; margin-right: 75px; color: white">MOOD</a>
                            <a class="navbar-brand" href="#" style="font-size: inherit; margin-right: 75px; color: white">BLOG</a>
                            <a class="navbar-brand" href="#" style="font-size: inherit; margin-right: 75px; color: white">CONTACT</a>
                            <a class="navbar-brand" href="#" style="font-size: inherit; margin-right: 75px; color: white"><i class="fas fa-search"></i></a>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
        <div class="row" style="margin-top: 100px;">
            <div class="col-lg-12">
                <div style="text-align: center;">
                    <p style="font-size: 70px;margin-top: 70px;color: white;font-family: -webkit-body;">Life begins after Coffee.</p>
                </div>
                <div style="text-align: center;">
                    <a href="#menu"><button style="background-color: #f2c46d;color: white;height: 50px;width: 200px;margin-top: 470px; border: none;">View Menu</button></a>
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top: 40px;margin-bottom: 100px;width: 1360px;">
        <div class="col-lg-12" style="text-align: center;">
            <p id="menu" style="font-size: 45px; color: black;font-family: -webkit-body;">What would you like to have?</p>
            <p>Coffee plunger pot sweet barista, grounds acerbic coffee instant crema cream in half and half. Spoon lungo</p>
            <p>variety, as, siphon, ristretto, iced brewed and acerbic affogato grinder.</p>
        </div>
    </div>
    <div class="row" style="margin-top: 40px;margin-bottom: 100px;width: 1360px;">
        <div class="col-lg-12">
            <div class="row" style="margin-left: 168px;margin-right: 128px;text-align: center;">
                <?php foreach ($data as $key => $value): ?>
                    <div class="col-md-2" style="background-image: url(src/images/<?=$value['bg_url']?>);background-repeat: no-repeat;background-size: cover;background-position: center;height: 500px;width: 250px;margin-right: 10px; opacity: 80%;">
                        <p style="color: white; margin-top: 250px;font-size: x-large;"><?=$value['title']?></p>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top: 40px;margin-bottom: 100px;width: 1360px;">
        <div class="col-lg-12" style="text-align: center;">
            <p style="font-size: 40px; color: black;font-family: -webkit-body;">Extraction instant that variety</p> 
            <p style="font-size: 40px; color: black;font-family: -webkit-body;">white robusta strong</p> 
            <p>Coffee plunger pot sweet barista, grounds acerbic coffee instant crema cream in half and half. Spoon lungo</p>
            <p>variety, as, siphon, ristretto, iced brewed and acerbic affogato grinder. Mazagran café au lait wings spoon,</p>
            <p>percolator milk latte dark strong. Whipped, filter latte, filter aromatic grounds doppio caramelization half and half.</p>
            <div style="text-align: center;">
                <button style="background-color: #f2c46d;color: white;height: 50px;width: 200px;margin-top: 50px; border: none;">Contact Us</button>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top: 40px;width: 1360px;">
        <div class="col-lg-12" style="text-align: center;background-color: #f2f2f2;height: 190px;">
            <p style="font-size: 40px;color: black;font-family: -webkit-body;margin-bottom: 40px;margin-top: 70px;">Health Benefits of Coffee</p> 
        </div>
    </div>
    <div class="row" style="margin-bottom: 100px;width: 1360px; height: 220px;">
        <div class="col-lg-4" style="text-align: center; background-color: #f2f2f2;">
            <img src="src/images/battery-full.svg" style="height: 60px;width: 60px;margin-bottom: 20px;">
            <p style="font-size: 20px;">BOOST ENERGY LEVEL</p>
        </div>
        <div class="col-lg-4" style="text-align: center; background-color: #f2f2f2;">
            <img src="src/images/sun.svg" style="height: 60px;width: 60px;margin-bottom: 20px;">
            <p style="font-size: 20px;">REDUCE DEPRESSION </p>
        </div>
        <div class="col-lg-4" style="text-align: center; background-color: #f2f2f2;">
            <img src="src/images/weight.svg" style="height: 60px;width: 60px;margin-bottom: 20px;">
            <p style="font-size: 20px;">AID IN WEIGHT LOSS</p>
        </div>
    </div>
    <div class="row" style="margin-top: 40px;margin-bottom: 100px;width: 1360px;">
        <div class="col-lg-6" style="text-align: end;margin-right: 100px;">
            <img src="src/images/blog-1.jpg" style=" max-width: 60%; height: auto;">
        </div>
        <div class="col-lg-4" style="text-align: start;">
            <p style="font-weight: 500; font-size: xx-large;">BLOG</p>
            <p style="font-weight: 600; font-size: 42px;">Qui espresso, grounds </p>
            <p style="font-weight: 600; font-size: 42px;">to go</p>
            <p style="font-size: larger;font-weight: 400;">December 12, 2019 | Espresso</p>
            <p>
                Skinny caffeine aged variety filter saucer redeye, sugar
                sit steamed eu extraction organic. Beans, crema half
                and half fair trade carajillo in a variety dripper doppio
                pumpkin spice cup lungo, doppio, est trifecta breve and,
                rich, extraction robusta a eu instant. Body sugar
                steamed, aftertaste, decaffeinated coffee fair trade sit,
                white shop fair trade galão, dark crema breve
                frappuccino iced strong siphon trifecta in a at viennese.
            </p>
            <a style="font-size: 17px;font-weight: 600;">READ MORE <i class="fas fa-arrow-right"></i></a>
        </div>
    </div>
    <div class="row" style="margin-top: 30px;width: 1360px;background-color: #2e2e2e;height: 220px;">
        <div class="col-lg-3" style="text-align: end;margin-right: 50px; margin-left: -10px; margin-top: 60px;">
            <img src="src/images/logo.svg" alt="" srcset="" style="margin-left: 80px; width: 80px; height: 80px;">
        </div>
        <div class="col-lg-4" style="margin-top: 60px;">
            <p style="color: white;">2800 S White Mountain Rd | Show Low AZ 85901</p>
            <p style="color: white;">(928) 537-1425 | info@grinder-coffee.com</p>
            <div class="row">
                <div class="col-sm-1" style="margin-right: 10px">
                    <a href="" style="color: white;font-size: xx-large;"><i class="fab fa-instagram"></i></a>
                </div>
                <div class="col-sm-1">
                    <a href="" style="color: white;font-size: xx-large;"><i class="fab fa-facebook-square"></i></i></a>
                </div>
            </div>
        </div>
        <div class="col-lg-4" style="margin-top: 60px;">
            <p style="color: white;">NEWSLETTER</p>
            <form class="row g-3">
                <div class="col-auto">
                    <input type="text" placeholder="YOUR EMAIL ADDRESS" style="height: 35px;width: 200px;color: white;background-color: #2e2e2e;font-size: 12px;">
                </div>
                <div class="col-auto">
                    <button style="background-color: #f2c46d;color: white;height: 35px;width: 110px; border: none; margin-left: -17px;">SUBSCRIBE</button>
                </div>
            </form>
            
        </div>
    </div>
    
</body>

<script src="dist/bootstrap.bundle.js"></script>
<script src="src/jquery.js"></script>
<script>
    $(function () {
        $(document).scroll(function () {
            var $nav = $(".navbar-fixed-top");
            $nav.toggleClass('scrolled', $(this).scrollTop() > $nav.height());
        });
    });
</script>
</html>