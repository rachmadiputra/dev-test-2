let mix = require('laravel-mix');

mix.js('src/bootstrap.bundle.js', 'dist')
   .js('src/jquery.js', 'dist')
   .sass('src/bootstrap.scss', 'dist')
   .sass('src/font-crimson.scss', 'dist')
   .sass('src/index_style.scss', 'dist');